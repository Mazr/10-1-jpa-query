package com.twuc.webApp.domain;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, String> {

    List<Product> findAllByProductLineTextDescriptionContains(String s);

    List<Product> getAllByQuantityInStockBetween(short i, short i1);
    Page<Product> getAllByQuantityInStockBetween(short i, short i1, Pageable pageable);

    List<Product> getAllByQuantityInStockBetween(Short quantityInStock, Short quantityInStock2);
    // TODO
    //
    // 如果需要请在此添加方法。
    //
    // <--start--
    // --end-->
}